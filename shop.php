<?php
    session_start();
    $id = SESSION_ID();

    include("db.php");
    $result_check = $db -> query("SELECT user_id FROM session WHERE session_id='$id'");
    $checkrow = mysqli_fetch_row($result_check);
    if($result_check->num_rows != 0){
        $user_id = $checkrow[0];
    }

    $category = $db -> query("SELECT id,name FROM category");
    $items = $db -> query("SELECT id,img,name,category_id,price,type,amount FROM items");
?>
<meta charset="UTF-8">
<link rel="stylesheet" href="reset.css?<?php echo filemtime('reset.css') ?>"/>
<link rel="stylesheet" href="header.css?<?php echo filemtime('header.css') ?>"/>
<link rel="stylesheet" href="main.css?<?php echo filemtime('main.css') ?>"/>
<link rel="stylesheet" href="footer.css?<?php echo filemtime('footer.css') ?>"/>
<link rel="stylesheet" href="shop.css?<?php echo filemtime('main.css') ?>"/>
<title>ProjectZ</title>
<?php
	include("header.php");
	include("promo.php");
?>

<script>
    function After(){
        var login_block = document.getElementsByClassName("login-block")[0];
        login_block.style.transform = "scale(1.1)";
    }

    function Before(){
        var login_block = document.getElementsByClassName("login-block")[0];
        login_block.style.transform = "scale(1.0)";
    }

    function ViewLogin(){
        var login_block = document.getElementsByClassName("login-block")[0];
        location.href='#promo';
        login_block.style.transition = "transform .2s cubic-bezier(0.68, -3.67, 0.37, 3.98)";
        setTimeout("After()", 100);
        setTimeout("Before()", 350);
    }

    function setFilter(category) {
        var items = document.getElementsByClassName('item');
        for (var i = 0; i < items.length; i++)
            items[i].style.display=(category == -1 || items[i].getAttribute("category") == category ? "block" : "none");
    }

    function select() {
        setFilter($(".shop-category").val());
    }
</script>

<div class="main">
	<div class="shop">
	    <p class="shop-text">Магазин</p>
	    <div class="shop-block">
    		<select class="shop-category" onchange="select()">
			<option value="-1" selected>Все товары</option>
		    <?php
		    	while($key = mysqli_fetch_row($category)) {
		    		echo "<option value=\"$key[0]\">$key[1]</option>";
		    	}
		    ?>
			</select>
	    	<div class="shop-button" <?php if(isset($user_id)) { echo"onclick=\"location.href='lc.php#cart'\""; } else { echo"onclick=\"ViewLogin()\""; } ?>
	    		>Перейти в корзину
	    	</div>
	    </div>
		<div class="items">
<?php
    if (isset($user_id)){
	    $user_status = $db -> query("SELECT status FROM user WHERE id='$user_id'");

	    $status = mysqli_fetch_row($user_status)[0];

	    if ($status == "admin"){
?>
			<img class="item-add" src="items/default.png" onClick="location.href='item-form.php'">
<?php
	    }
	}

	while($item = mysqli_fetch_row($items)){
		$id = $item[0];
		$img = $item[1];
		$name = $item[2];
		$category_id = $item[3];
		$price = $item[4];
		$type = $item[5];
		$amount = $item[6];
?>
	<div class="item" category="<?php echo $category_id; ?>" onClick="location.href='item.php?id=<?php echo $id;?>'">
		<p class="item-text">
			<?php echo $name; ?>
		</p>
		<img class="item-img" src="items/<?php echo $img; ?>.png">
		<p class="item-price">
			<?php echo $price; ?><span style="color:green">$</span>
		</p>
		<?php
			if($amount > 1){
		?>
		<p class="item-amount">
			за <?php echo $amount; ?>шт.
		</p>
		<?php
			}
		?>
	</div>
<?php
	}

?>
		</div>
	</div>
<?
    include("sidebar.php");
?>
</div>
<?php
    include("footer.php");
?>