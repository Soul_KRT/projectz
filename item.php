<?php
    session_start();
    $id = SESSION_ID();

    if(isset($_GET['id'])){
		$item_id = $_GET['id'];
    	include("db.php");
    	$item = mysqli_fetch_row($db -> query("SELECT img,name,category_id,price,type,amount FROM items WHERE id='$item_id'"));
		$img = $item[0];
		$name = $item[1];
		$category_id = $item[2];
		$price = $item[3];
		$type = $item[4];
		$amount = $item[5];

		$category = mysqli_fetch_row($db -> query("SELECT name FROM category WHERE id='$category_id'"))[0];
    } else {
?>
	<script>
		location.href="index.php";
	</script>
<?php
    }
?>
<script>
    function After(){
        var login_block = document.getElementsByClassName("login-block")[0];
        login_block.style.transform = "scale(1.1)";
    }

    function Before(){
        var login_block = document.getElementsByClassName("login-block")[0];
        login_block.style.transform = "scale(1.0)";
    }

    function ViewLogin(){
        var login_block = document.getElementsByClassName("login-block")[0];
        location.href='#promo';
        login_block.style.transition = "transform .2s cubic-bezier(0.68, -3.67, 0.37, 3.98)";
        setTimeout("After()", 100);
        setTimeout("Before()", 350);
    }

	function calcPrice(price)
	{
		document.getElementById("price").innerHTML = (price * Math.max(Math.floor($("#quantity").val()), 0)).toFixed(2);
	}
</script>
<meta charset="UTF-8">
<link rel="stylesheet" href="reset.css?<?php echo filemtime('reset.css') ?>"/>
<link rel="stylesheet" href="header.css?<?php echo filemtime('header.css') ?>"/>
<link rel="stylesheet" href="main.css?<?php echo filemtime('main.css') ?>"/>
<link rel="stylesheet" href="footer.css?<?php echo filemtime('footer.css') ?>"/>
<link rel="stylesheet" href="item.css?<?php echo filemtime('main.css') ?>"/>
<title>ProjectZ</title>
<?php
	include("header.php");
	include("promo.php");
?>
<div class="main">
	<div class="content">
		<div class="item">
	        <p class="item-text"><?php echo $name; ?></p>
			<div class="item-block">
				<img class="item-img" src="items/<?php echo $img; ?>.png">
				<div class="item-data">
					<p class="field_lable_black">
						<strong>ID предмета: </strong>
						<?php echo $type; ?>
					</p>
					<p class="field_lable_black">
						<strong>Количество: </strong>
						<?php echo $amount; ?>шт.
					</p>
					<p class="field_lable_black">
						<strong>Цена: </strong>
						<?php echo $price; ?><span style="color:green">$</span>
					</p>
					<p class="field_lable_black">
						<strong>Категория: </strong>
						<?php echo $category; ?>
					</p>
				</div>
		    </div>
			<div class="item-form">
				<p class="buy-text">Покупка</p>
				<form method="post" id="form">
					<p><input type="number" class="item-field" placeholder="количество" value="1" min="1" name="quantity" id="quantity" onkeyup="calcPrice(<?php echo $price; ?>)" onchange="calcPrice(<?php echo $price; ?>)"></p>
					<?php
						if($amount>1){
							echo "<p class=\"amount-text\">по $amount штук</p>";
						}
					?>
					<span class="price-text"><span class="field_lable_black"><strong>Сумма покупки: </strong></span><span id="price"><?php echo $price; ?></span><span style="color:green">$</span></span>
			        <input type="submit" class="item-button" value="Купить">
			    </form>
		        <strong><div class="buy-errors"></div></strong>
	    	</div>
	    </div>
	</div>
<?
    include("sidebar.php");
?>
</div>
<?php
    include("footer.php");
?>

<script>
	$("#form").submit(function(event) {
    event.preventDefault();
    $.post('item-buy.php', {
        user_id:
		    <?php echo isset($user_id)?$user_id:"\"null\""; ?>,
        item_id: <?php echo $item_id; ?>,
        quantity: $("#quantity").val()
    }, function(data, textStatus, xhr) {
        switch(data){
        case "error-user_null":
            document.getElementsByClassName("buy-errors")[0].innerHTML="Авторизируйтесь!";
            ViewLogin();
            break;
        case "error-buy1":
            document.getElementsByClassName("buy-errors")[0].innerHTML="Введено неверное количество!";
            break;
        case "error-buy2":
            document.getElementsByClassName("buy-errors")[0].innerHTML="Недостаточно средств!";
            break;
        case "buyed":
            document.getElementsByClassName("buy-errors")[0].style.color="green";
            document.getElementsByClassName("buy-errors")[0].innerHTML="Товар успешно приобретен и добавлен в корзину";
			setTimeout("window.location.reload()", 250);
            break;
        case "fatal_error":
            document.getElementsByClassName("buy-errors")[0].innerHTML="Ошибка! Товар не куплен.";
            break;
        }
    });
 	});
</script>