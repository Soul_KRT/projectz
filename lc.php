<?php
    session_start();
    $id = SESSION_ID();
?>

<?php
    include("db.php");
    $result_check = $db -> query("SELECT user_id FROM session WHERE session_id='$id'");
    $checkrow = mysqli_fetch_row($result_check);
    if($result_check->num_rows != 0){
        $user_id = $checkrow[0];
    }
    if (!isset($user_id)){
        exit("<script>
            document.location.href='index.php'
        </script>");
    }
    $user_data = $db -> query("SELECT mail,nick,skin,balance,verify,mail_hash,firstjoin FROM user WHERE id='$user_id'");
    $user_row = mysqli_fetch_row($user_data);

    $user_sessions = $db -> query("SELECT count(id) FROM session WHERE user_id='$user_id'");
    $session_row = mysqli_fetch_row($user_sessions);

    $cart = $db -> query("SELECT item_id,quantity FROM cart WHERE user_id='$user_id'");

?>
<meta charset="UTF-8">
<link rel="stylesheet" href="reset.css?<?php echo filemtime('reset.css') ?>"/>
<link rel="stylesheet" href="header.css?<?php echo filemtime('header.css') ?>"/>
<link rel="stylesheet" href="main.css?<?php echo filemtime('main.css') ?>"/>
<link rel="stylesheet" href="footer.css?<?php echo filemtime('footer.css') ?>"/>
<link rel="stylesheet" href="lc.css?<?php echo filemtime('lc.css') ?>"/>
<title>ProjectZ</title>
<script src="jquery-3.5.1.min.js"></script>
<script>
    function destroy(){
        $.post('destroy.php', {
            user_id: <?php echo $user_id; ?>,
            session_id: "<?php echo $id; ?>"
        }, function(data, textStatus, xhr){
            window.location.reload();
        });
    }

    function set_black(){
        document.getElementsByClassName("block-skin")[0].style.background="black";
    };

    function set_white(){
        document.getElementsByClassName("block-skin")[0].style.background="white";
    };

    function verify(){
        $.post('verify.php', {
            id: '<?php echo $user_id?>',
            mail: '<?php echo $user_row[0]?>',
            nick: '<?php echo $user_row[1]?>'
        }, function(data, textStatus, xhr) {
            alert("Что бы подтвердить Email, перейдите по ссылке http://projectz/verify.php?mail_hash="+data);
        });
    };
</script>

<?php
    ini_set('upload_max_filesize', '1M');
    if ($_SERVER['REQUEST_METHOD'] == "POST" ) {
        if ($_FILES['skin']['error'] == UPLOAD_ERR_OK && $_FILES['skin']['type'] == 'image/png') {
            $skin = "skins/".$user_row[1].".png";
            move_uploaded_file($_FILES['skin']["tmp_name"], $skin);
            $db -> query("UPDATE user set skin = '$user_row[1]' where id='$user_id'");
            unset($_FILES);
            $user_row[2] = $user_row[1];
        }
    }
    include("header.php");
    include("promo.php");
?>

<div class="main">
<style>
.blocks {
    display: none;
}
.block{
    display: none;
}
.blocks:target+.block {
    display: block;
}
</style>

<div class="content">
    <div class="profile">
        <div class="lc-blocks">
                <a href="#profile">Мой профиль</a>
                <a href="#settings">Настройки</a>
                <a href="#skin">Скин</a>
                <a href="#cart">Корзина</a>
        </div>
        <hr style="margin:15px;">
        <a class="blocks" id="profile"></a>
        <div class="block">
            <p class="profile_text">Профиль</p>
            <?php
                include("SkinViewer2D.php");
            ?>
            <div class="profile-block">
                <img class="profile-block-avatare" src="SkinViewer2D.php?show=head&file_name=skins/<?php echo $user_row[2] ?>">
                <div class="profile-block-data">
                    <p>Ник: <?php echo $user_row[1] ?></p>
                    <p>Email: <?php echo $user_row[0] ?></p>
                    <p>Баланс: <?php echo $user_row[3] ?><span style="color:green;">$</span></p>
                    <p>Дата регистрации: <?php echo $user_row[6] ?></p>
                    <?php if($user_row[4]==0){
                        ?> <p>Почта: не подтверждена <div class="button-verify" onclick="verify()">Подтвердить</div></p><?php
                    } else{ ?> <br><p>Почта: подтверждена</p> <?php } ?>
                </div>
            </div>
        </div>

        <a class="blocks" id="settings"></a>
        <div class="block">
            <p class="profile_text">Поменять пароль</p>
            <div class="cp-block">
                <div class="change-password">
                    <form method="post" id="form-cp_confirm">
                        <span class="field_lable_black">Старый пароль:</span>
                        <span class="field_lable_red">*</span>
                        <input class="cp-field" id="old_pass" type="text" maxlength="16">
                        <span class="field_lable_black">Новый пароль:</span>
                        <span class="field_lable_red">*</span>
                        <input class="cp-field" id="pass1" type="password" maxlength="16">
                        <span class="field_lable_black">Повторите пароль:</span>
                        <span class="field_lable_red">*</span>
                        <input class="cp-field" id="pass2" type="password" maxlength="16">
                        <input type="submit" name="submit" class="button-confirm" value="Подтвердить">
                    </form>
                    <strong><div class="error"></div></strong>
                </div>
            </div>
            <p class="profile_text" style="margin-top: 50px;">Доступ к аккаунту</p>
            <div class="cp-block">
                <div class="change-password">
                    <p>Если Вы подозреваете, что кто-то получил доступ к Вашему аккаунту или Вы просто забыли выйти из своего аккаунта на каком-то другом устройстве, то Вы можете моментально закрыть все активные сессии на своем аккаунте.</p>
                    <br>
                    <p>Активных сессий: <?php echo $session_row[0]; ?></p>
                    <div class="button-destroy" onclick="destroy()">Закрыть все активные сессии</div>
                </div>
            </div>
        </div>

        <a class="blocks" id="skin"></a>
        <div class="block">
            <p class="profile_text">Ваш скин</p>
            <div class="block-skin">
                <div class="block-skin_buttons">
                    <div class="set_black" onclick="set_black()"></div>
                    <div class="set_white" onclick="set_white()"></div>
                </div>
                <div class="block-skins">
                    <img class="skin" src="SkinViewer2D.php?show=body&side=front&cloak=hd&mode=hd&file_name=skins/<?php echo $user_row[2] ?>">

                    <img class="skin" src="SkinViewer2D.php?show=body&side=back&cloak=hd&mode=hd&file_name=skins/<?php echo $user_row[2] ?>">
                </div>
            </div>
            <form method="post" class="block-skin_form" enctype="multipart/form-data">
                    <input type="file" name='skin' class="button-select" accept="image/png">
                    <input type="submit" class="button-change" value="Change">
            </form>
        </div>
        <a class="blocks" id="skin"></a>
        <div class="block">
            <p class="profile_text">Ваш скин</p>
            <div class="block-skin">
                <div class="block-skin_buttons">
                    <div class="set_black" onclick="set_black()"></div>
                    <div class="set_white" onclick="set_white()"></div>
                </div>
                <div class="block-skins">
                    <img class="skin" src="SkinViewer2D.php?show=body&side=front&cloak=hd&mode=hd&file_name=skins/<?php echo $user_row[2] ?>">

                    <img class="skin" src="SkinViewer2D.php?show=body&side=back&cloak=hd&mode=hd&file_name=skins/<?php echo $user_row[2] ?>">
                </div>
            </div>
            <form method="post" class="block-skin_form" enctype="multipart/form-data">
                    <input type="file" name='skin' class="button-select" accept="image/png">
                    <input type="submit" class="button-change" value="Change">
            </form>
        </div>
        <a class="blocks" id="cart"></a>
        <div class="block">
            <p class="profile_text">Корзина</p>
            <p class="cart_text">После покупки в магазине, все предметы попадают сюда. Забрать предмет из корзины и получить его в игре можно с помощью специальной команды: <p>/shop [тип предмета] [количество]</p></p>
            <p class="profile_text" style="margin-top: 30px;">Содержимое корзины:</p>
            <table class="block-cart">
                <tr class="cart_item-tr">
                    <td class="cart_item-td" colspan="2">Предмет</td>
                    <td class="cart_item-td">Тип предмета</td>
                    <td class="cart_item-td">Количество</td>
                </tr>
                <?php
                    while($cart_item = mysqli_fetch_row($cart)){

                        $item_id = $cart_item[0];
                        $quantity = $cart_item[1];

                        $items = $db -> query("SELECT img,name,type,amount FROM items WHERE id='$item_id'");
                        $item = mysqli_fetch_row($items);
                        $img = $item[0];
                        $name = $item[1];
                        $type = $item[2];
                        $amount = $item[3];
                        ?>
                        <tr>
                            <td class="cart_item-td"><img class="cart_img" src="items/<?php echo $img; ?>.png"></td>
                            <td class="cart_item-td"><?php echo $name; ?></td>
                            <td class="cart_item-td"><?php echo $type; ?></td>
                            <td class="cart_item-td"><?php echo $amount*$quantity; ?></td>
                            <td>
                            <hr class="cart_hr"></td>
                        </tr>
                        <?php
                    }
                ?>
            </table>
        </div>
    </div>
</div>
<?php
    include("sidebar.php");
?>
</div>
<?php
    include("footer.php");
?>

<script>
    $("#replace").click(()=>location.href="index.php");
    document.getElementById("replace").innerHTML="Главная страница";

    $("#form-cp_confirm").submit(function(event) {
    event.preventDefault();
    $.post('Change-Password.php', {
        user_id: <?php echo $user_id; ?>,
        old_pass: $("#old_pass").val(),
        pass1: $("#pass1").val(),
        pass2: $("#pass2").val()
    }, function(data, textStatus, xhr) {
        switch(data){
        case "error1":
            document.getElementsByClassName("error")[0].innerHTML="Вы ввели не всю информацию, заполните все поля!";
            break;
        case "error2":
            document.getElementsByClassName("error")[0].innerHTML="Неверный старый пароль!";
            break;
        case "error3":
            document.getElementsByClassName("error")[0].innerHTML="Новые пароли не совпадают!";
            break;
        case "ErrorSmall":
            document.getElementsByClassName("error")[0].innerHTML="Введенный вами пароль слишком короткий!";
            break;
        case "ErrorBig":
            document.getElementsByClassName("error")[0].innerHTML="Введенный вами пароль слишком длинный!";
            break;
        case "confirm":
            document.getElementsByClassName("error")[0].innerHTML="Пароль успешно изменён!";
            document.getElementsByClassName("error")[0].style.color="green";
            setTimeout("destroy()", 250);
            break;
        case "fatal_error":
            document.getElementsByClassName("error")[0].innerHTML="Ошибка! Пароль не изменён.";
            break;
        }
    });
    });
</script>