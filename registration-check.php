<?php
    session_start();
    $id = SESSION_ID();
?>
<meta charset="UTF-8">
<link rel="stylesheet" href="reset.css?<?php echo filemtime('reset.css') ?>"/>
<link rel="stylesheet" href="header.css?<?php echo filemtime('header.css') ?>"/>
<link rel="stylesheet" href="footer.css?<?php echo filemtime('footer.css') ?>"/>
<link rel="stylesheet" href="registration.css?<?php echo filemtime('registration.css') ?>"/>
<link rel="stylesheet" href="main.css?<?php echo filemtime('main.css') ?>"/>
<title>ProjectZ</title>
<script src="jquery-3.5.1.min.js"></script>
<?php
    include("header.php");
    include("promo.php");
?>
<div class="main">
<?php
    include("registration.php");
    include("sidebar.php");
?>
</div>
<?php
    include("footer.php");
?>
