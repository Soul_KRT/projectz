-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Ноя 23 2021 г., 00:43
-- Версия сервера: 8.0.19
-- Версия PHP: 8.0.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `projectz`
--

-- --------------------------------------------------------

--
-- Структура таблицы `cart`
--

CREATE TABLE `cart` (
  `id` int NOT NULL,
  `user_id` int NOT NULL,
  `item_id` int NOT NULL,
  `quantity` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `cart`
--

INSERT INTO `cart` (`id`, `user_id`, `item_id`, `quantity`) VALUES
(9, 1, 6, 18),
(10, 3, 6, 14),
(11, 5, 6, 2),
(12, 1, 7, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `category`
--

CREATE TABLE `category` (
  `id` int NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `category`
--

INSERT INTO `category` (`id`, `name`) VALUES
(1, 'Патроны'),
(2, 'Оружие');

-- --------------------------------------------------------

--
-- Структура таблицы `items`
--

CREATE TABLE `items` (
  `id` int NOT NULL,
  `img` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `name` varchar(32) NOT NULL,
  `lore` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `category_id` int NOT NULL,
  `price` double NOT NULL,
  `type` varchar(32) NOT NULL,
  `amount` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `items`
--

INSERT INTO `items` (`id`, `img`, `name`, `lore`, `category_id`, `price`, `type`, `amount`) VALUES
(6, '6Пуля 9x19', 'Пуля 9x19', NULL, 1, 0.99, 'BULLET_9x19', 10),
(7, '7M4A1', 'M4A1', NULL, 2, 10.99, 'GUN_M4A1', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `session`
--

CREATE TABLE `session` (
  `id` int NOT NULL,
  `user_id` int NOT NULL,
  `session_id` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `session`
--

INSERT INTO `session` (`id`, `user_id`, `session_id`) VALUES
(40, 1, '9onu948r6h2ht7qqddm0mb0rp0tks3mh'),
(41, 3, 'jj1i1musb7rspp7t05k9a0up4t1rda9m'),
(42, 3, 'jj1i1musb7rspp7t05k9a0up4t1rda9m'),
(43, 3, 'jj1i1musb7rspp7t05k9a0up4t1rda9m'),
(45, 1, 'v6v38e7gfihg3rdkpnokoau29pjiek5e'),
(46, 2, '0rkmd1uld7kj4h58cm1o0qru387ho1us'),
(48, 5, '52klv6hine9mkuip852vs1s246007o4m');

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int NOT NULL,
  `mail` varchar(32) NOT NULL,
  `nick` varchar(16) NOT NULL,
  `pass` varchar(128) NOT NULL,
  `skin` varchar(64) NOT NULL,
  `status` varchar(16) NOT NULL,
  `balance` double NOT NULL,
  `verify` tinyint(1) NOT NULL,
  `mail_hash` varchar(128) NOT NULL,
  `firstjoin` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `mail`, `nick`, `pass`, `skin`, `status`, `balance`, `verify`, `mail_hash`, `firstjoin`) VALUES
(1, 'soul.krt.00@mail.ru', 'Soul_KRT', '5ba77ecf4c3bf1943d3fa250efc3c4a4', 'Soul_KRT', 'admin', 951.19, 1, 'c7371281ed0d73f58567d8ffe21c54f5', '2021-11-15'),
(2, 'zkoryakin12@mail.ru', 'f3a3', '720e1eb48ac2cb90696f9133dcc3bc13', 'default', 'user', 0, 0, 'c7427c160fa5b7349b0279813f52b999', '2021-11-15'),
(3, '1_d_tkach@mail.ru', 'asdasd', 'a8f5f167f44f4964e6c998dee827110c', 'default', 'admin', 2.03, 0, 'c3fbc5452caa8fbd0793a2f612b76e81', '2021-11-16'),
(4, 'soul.krt.14@mail.ru', 'Soul14', '5ba77ecf4c3bf1943d3fa250efc3c4a4', 'default', 'user', 0, 0, '49c212146eac7a6639f329c108dc070e', '2021-11-17'),
(5, 'qwertyuiopasdfgh@mail.ru', 'qwertyuiopasdfgh', '1ebfc043d8880b758b13ddc8aa1638ef', 'default', 'user', 4998.02, 0, '4648969fb10ec9c16b2848a139b622d2', '2021-11-22');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `session`
--
ALTER TABLE `session`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT для таблицы `category`
--
ALTER TABLE `category`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `items`
--
ALTER TABLE `items`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблицы `session`
--
ALTER TABLE `session`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
