<?php
	if (isset($_POST['nickname'])) {
        $nickname = $_POST['nickname'];
        if ($nickname == '')
        {
            unset($nickname);
            exit("CheckNull");
        }

        $nickname = stripslashes($nickname);
        $nickname = htmlspecialchars($nickname);
        $nickname = trim($nickname);

        if(!preg_match("/^[A-Za-z][A-Za-z0-9_]{1,16}$/", $nickname)){
            exit("CheckInvalid");
        }

        if(mb_strlen($nickname) < 4) {
        	exit("CheckSmall");
        }

        if(mb_strlen($nickname) > 16) {
        	exit("CheckBig");
        }

        include ("db.php");

    	$result = $db -> query("SELECT id FROM user WHERE nick='$nickname'");
        $myrow = mysqli_fetch_row($result);
        if ($result -> num_rows > 0) {
        	exit("CheckFalse");
      	} else {
      		exit("CheckTrue");
    	}
    }
?>

<script>
    document.location.href='index.php';
</script>