<div class="login-block">
    <p class="login-block-text">Вход</p>
    <div class="login-block-items">
        <form method="post" id="form-login">
            <p  class="login-block_text">Логин</p>
            <p><input class="login-block-lp" id="user_login" type="text" maxlength="16"></p>
            <p class="login-block_text">Пароль</p>
            <p><input class="login-block-lp" id="user_password" type="password" maxlength="16"></p>
            <input type="submit" name="submit" class="buttons-sidebar" value="Войти">
        </form>
    <div class="login-block-subbuttons">
        <div class="login-subbuttons" onclick="location.href='registration-check.php'">Регистрация</div>
        <div class="login-subbuttons" onclick="location.href='restore.php'">Восстановить пароль</div>
    </div>
</div>
    <strong><p class="login-errors"></p></strong>
</div>

<script>
    $("#form-login").submit(function(event) {
    event.preventDefault();
        $.post('login.php', {
            user_login: document.getElementById("user_login").value,
            user_password: document.getElementById("user_password").value
        }, function(data, textStatus, xhr) {
            switch(data){
                case "login-error1":
                    document.getElementsByClassName("login-errors")[0].innerHTML="Заполните все поля авторизации!";
                break;
                case "login-error2":
                    document.getElementsByClassName("login-errors")[0].innerHTML="Неверный логин или пароль!";
                break;
                case "login":
                    document.location.href='lc.php#profile';
                break;
        }
    });
    });
</script>