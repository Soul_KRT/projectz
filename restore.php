<?php
    session_start();
    $id = SESSION_ID();
    include("db.php");
    $result_check = $db -> query("SELECT user_id FROM session WHERE session_id='$id'");
    $checkrow = mysqli_fetch_row($result_check);
    if($result_check->num_rows != 0){
        $user_id = $checkrow[0];
    }
    if (isset($user_id)){
        echo"<script>
            document.location.href='lc.php'
        </script>";
    }
?>

<meta charset="UTF-8">
<link rel="stylesheet" href="reset.css?<?php echo filemtime('reset.css') ?>"/>
<link rel="stylesheet" href="header.css?<?php echo filemtime('header.css') ?>"/>
<link rel="stylesheet" href="main.css?<?php echo filemtime('main.css') ?>"/>
<link rel="stylesheet" href="restore.css?<?php echo filemtime('restore.css') ?>"/>
<link rel="stylesheet" href="footer.css?<?php echo filemtime('footer.css') ?>"/>
<title>ProjectZ</title>
<script src="jquery-3.5.1.min.js"></script>
<?php
    include("header.php");
    include("promo.php");
?>
<div class="main">
<div class="restore-block">
<?php
    $restore = 0;
    if(isset($_GET['pass_hash'])) {
        $pass_hash = $_GET['pass_hash'];
        $pass_check = $db -> query("SELECT id FROM user WHERE pass = '$pass_hash'");
        $passrow = mysqli_fetch_row($pass_check);
        if($pass_check->num_rows > 0){
        $restore = 1;

?>
        <div class="restore restore_confirm">
            <strong><p class="restore-title">Восстановление пароля</p></strong>
            <p class="restore-attention">Задайте новый пароль для своего аккаунта.<br><br>Советуем придумать <b>пароль</b>, который будет состоять из букв нижнего и верхнего регистров. Минимальная длинна - шесть символов, а максимальная - 16 символов.</p>
            <form method="post" id="form-restore_confirm" class="restore_confirm-data">
                <span class="field_lable_black">Пароль:</span>
                <span class="field_lable_red">*</span>
                <br><input type="password" class="restore-field" id="password1_restore" name="password1_restore">
                <br><span class="field_lable_black">Повторите пароль:</span>
                <span class="field_lable_red">*</span>
                <br><input type="password" class="restore-field" id="password2_restore" name="password2_restore">
                <input type="submit" name="submit" class="restore_confirm-button" value="Подтвердить">
                <strong><div class="restore-errors"></div></strong>
            </form>
        </div>
<?php
        } else {
            ?>
            <script>
                document.location.href='index.php';
            </script>
            <?php
        }
    }
?>

<?php
    if(!$restore) {
?>
        <div class="restore">
            <strong><p class="restore-title">Восстановление пароля</p></strong>
            <p class="restore-attention">Для восстановления пароля, введите зарегистрированный ник или email. Вам будет отправлено письмо, перейдя по ссылке в котором Вы сможете задать новый пароль.</p>
            <form id="form-restore" method="post" class="restore-data">
                <input type="text" class="restore-field" id="login" name="login">
                <input type="submit" name="submit" class="restore-button" value="Восстановить">
            </form>
            <strong><div class="restore-errors"></div></strong>
        </div>
<?php
    }
?>
</div>
<?php
    include("sidebar.php");
?>
</div>
<?php
    include("footer.php");
?>

<script>
    $("#form-restore").submit(function(event) {
    event.preventDefault();
    $.post('restore-action.php', {
        login: $("#login").val()
    }, function(data, textStatus, xhr) {
        if (data == "no_login") {
            document.getElementsByClassName("restore-errors")[0].innerHTML='Пользователь с таким логином не найден!';
        } else {
            document.getElementsByClassName("restore-errors")[0].style.color="green";
            document.getElementsByClassName("restore-errors")[0].innerHTML='Письмо отправлено вам на почту!';
            alert("Что бы восстановить пароль, перейдите по ссылке http://projectz/restore.php?pass_hash="+data);
            setTimeout("document.location.href='index.php'", 250);
        }
    });
    });

    $("#form-restore_confirm").submit(function(event) {
    event.preventDefault();
    $.post('restore-action.php', {
        id_restore: "<?php if(isset($passrow[0])){ echo $passrow[0]; }?>",
        password1_restore: $("#password1_restore").val(),
        password2_restore: $("#password2_restore").val()
    }, function(data, textStatus, xhr) {
        switch(data){
            case "restore_confirm":
                document.getElementsByClassName("restore-errors")[0].style.color="green";
                document.getElementsByClassName("restore-errors")[0].innerHTML='Ваш пароль успешно изменён!';
                setTimeout("document.location.href='index.php'", 250);
                break;
            case "error_restore1":
                document.getElementsByClassName("restore-errors")[0].innerHTML='Вы ничего не ввели!';
                break;
            case "error_restore2":
                document.getElementsByClassName("restore-errors")[0].innerHTML='Пароли не совпадают!';
                break;
            case "errorSmall_restore":
                document.getElementsByClassName("restore-errors")[0].innerHTML='Введенный вами пароль слишком короткий!';
                break;
            case "errorBig_restore":
                document.getElementsByClassName("restore-errors")[0].innerHTML='Введенный вами пароль слишком длинный!';
                break;
            case "error_restore":
                document.getElementsByClassName("restore-errors")[0].innerHTML='Ошибка! Ваш пароль не изменён!';
                break;
        }
    });
    });
</script>