<?php
    if(isset($_POST['login'])){
        $login = $_POST['login'];

        $login = stripslashes($login);
        $login = htmlspecialchars($login);
        $login = trim($login);

        if(empty($login)){
            exit("no_login");
        }

        include ("db.php");

        $login_check = $db -> query("SELECT mail,pass FROM user WHERE mail = '$login' or nick = '$login'");
        $loginrow = mysqli_fetch_row($login_check);
        if($login_check->num_rows > 0){
            $mail = $loginrow[0];
            $pass_hash = $loginrow[1];
            $headers  = "MIME-Version: 1.0\r\n";
            $headers .= "Content-type: text/html; charset=utf-8\r\n";
            $headers .= "To: <$mail>\r\n";
            $headers .= "From: <soul.krt.00@mail.ru>\r\n";

            $message = '
                    <html>
                    <head>
                    <title>Подтвердите Email</title>
                    </head>
                    <body>
                    <p>Что бы подтвердить Email, перейдите по <a href="http://projectz/index.php?pass_hash=' . $pass_hash . '">ссылке</a></p>
                    </body>
                    </html>
                    ';
            mail($mail, "Подтвердите Email на сайте", $message, $headers);
            exit($pass_hash);
        } else {
            exit("no_login");
            //Пользователь с таким логином не найден!;
        }
    }

    if(isset($_POST['id_restore']) && isset($_POST['password1_restore']) && isset($_POST['password2_restore'])){

        $id_restore = $_POST['id_restore'];
        $password1_restore = $_POST['password1_restore'];
        $password2_restore = $_POST['password2_restore'];

        if ($password1_restore == '') {
            unset($password1_restore);
        }

        if ($password2_restore == '') {
            unset($password2_restore);
        }

        if (empty($password1_restore) && empty($password2_restore))
        {
            exit ("error_restore1");
            //Вы ничего не ввели!
        } else if(empty($password1_restore) || empty($password2_restore)){
            exit ("error_restore2");
            //Пароли не совпадают!
        }

        $password1_restore = stripslashes($password1_restore);
        $password1_restore = htmlspecialchars($password1_restore);
        $password1_restore = trim($password1_restore);

        $password2_restore = stripslashes($password2_restore);
        $password2_restore = htmlspecialchars($password2_restore);
        $password2_restore = trim($password2_restore);

        if (empty($password1_restore) && empty($password2_restore))
        {
            exit ("error_restore1");
            //Вы ничего не ввели!
        } else if(empty($password1_restore) || empty($password2_restore)){
            exit ("error_restore2");
            //Пароли не совпадают!
        }

        if ($password1_restore!=$password2_restore)
        {
            exit ("error_restore2");
            //Пароли не совпадают!
        }

        if(strlen($password1_restore) < 6){
            exit ("errorSmall_restore");
            //Введенный вами пароль слишком короткий!
        }

        if(strlen($password1_restore) > 16){
            exit ("errorBig_restore");
            //Введенный вами пароль слишком длинный!
        }

        $password_restore = md5($password1_restore);

        include ("db.php");

        $result_restore = $db -> query("UPDATE user SET pass='$password_restore' WHERE id='$id_restore'");
        if ($result_restore) {
            $destroy = $db -> query("DELETE FROM session WHERE user_id='$id_restore'");
            exit ("restore_confirm");
            //Ваш пароль успешно изменён!
        } else {
            exit ("error_restore");
            //Ошибка! Ваш пароль не изменён!
        }
    }
?>

<script>
    document.location.href='index.php';
</script>