<?php
    session_start();
    $id = SESSION_ID();
?>

<?php
    include("db.php");
    $result_check = $db -> query("SELECT user_id FROM session WHERE session_id='$id'");
    $checkrow = mysqli_fetch_row($result_check);
    if($result_check->num_rows != 0){
        $user_id = $checkrow[0];
    }
    if (isset($user_id)){
        echo "<script>
            document.location.href='index.php'
        </script>";
    }
?>
<script>
    function CheckNickname() {
        $.post('CheckNickname.php', {
            nickname: document.getElementById("nickname").value
        }, function(data, textStatus, xhr) {
            switch(data){
                case "CheckNull":
                    document.getElementsByClassName("CheckResult")[0].innerHTML="Введите какой-то ник!";
                    document.getElementsByClassName("CheckResult")[0].style.color="red";
                break;
                case "CheckInvalid":
                    document.getElementsByClassName("CheckResult")[0].innerHTML="Недопустимый ник!";
                    document.getElementsByClassName("CheckResult")[0].style.color="red";
                break;
                case "CheckFalse":
                    document.getElementsByClassName("CheckResult")[0].innerHTML="Ник занят!";
                    document.getElementsByClassName("CheckResult")[0].style.color="red";
                break;
                case "CheckSmall":
                    document.getElementsByClassName("CheckResult")[0].innerHTML="Ник слишком короткий!";
                    document.getElementsByClassName("CheckResult")[0].style.color="red";
                break;
                case "CheckBig":
                    document.getElementsByClassName("CheckResult")[0].innerHTML="Ник слишком длинный!";
                    document.getElementsByClassName("CheckResult")[0].style.color="red";
                break;
                case "CheckTrue":
                    document.getElementsByClassName("CheckResult")[0].innerHTML="Ник свободен!";
                    document.getElementsByClassName("CheckResult")[0].style.color="green";
                break;
            }
        });
    }
</script>
<div class="registration-block">
    <div class="register-block">
        <div class="registration-block-text">
            <h1>Регистрация</h1>
            <h3>Здравствуйте, уважаемый посетитель нашего проекта!</h3>
            <span>
                Регистрация на ProjectZ позволит Вам стать частью сервера.
                Вы сможете авторизироваться в клиенте, изменить свой скин, а так-же аватар профиля.
                <br>В случае возникновения проблем с регистрацией, обратитесь к <a href="https://vk.com/Soul_KRT">разработчику</a> сайта.
            </span>
        </div>
        <div class="attention">
            <h3>Внимание!</h3>
            <span>
                Минимальная длина <b>ника</b> - четыре символа. Разрешены только латинские буквы, цифры и знак нижнего подчеркивания. Ник должен начинаться с буквы.<br>Максимальная длина 16 символов.
                <br><br>Советуем придумать <b>пароль</b>, который будет состоять из букв нижнего и верхнего регистров. Минимальная длинна - шесть символов, а максимальная - 16 символов.
                <br><br>Пожалуйста укажите <b>действительный Email адрес</b>, это позволит Вам использовать полный функционал проекта!
            </span>
        </div>
        <form method="post" id="form-register" class="registration-block-fields">
            <div class="registration-block-fields_item">
                <div class="registration-block-fields_lable">
                    <span class="field_lable_black">Логин:</span>
                    <span class="field_lable_red">*</span>
                </div>
                <div class="registration-block-field">
                    <input type="text" id="nickname" class="registration_field" maxlength="16" onchange="CheckNickname()">
                    <strong><div class="CheckResult"></div></strong>
                </div>
            </div>
            <div class="registration-block-fields_item">
                <div class="registration-block-fields_lable">
                    <span class="field_lable_black">Пароль:</span>
                    <span class="field_lable_red">*</span>
                </div>
                <div class="registration-block-field">
                    <input type="password" id="password1" class="registration_field" maxlength="16">
                </div>
            </div>
            <div class="registration-block-fields_item">
                <div class="registration-block-fields_lable">
                    <span class="field_lable_black">Повторите пароль:</span>
                    <span class="field_lable_red">*</span>
                </div>
                <div class="registration-block-field">
                    <input type="password" id="password2" class="registration_field" maxlength="16">
                </div>
            </div>
            <div class="registration-block-fields_item">
                <div class="registration-block-fields_lable">
                    <span class="field_lable_black">Ваш E-Mail:</span>
                    <span class="field_lable_red">*</span>
                </div>
                <div class="registration-block-field">
                    <input type="text" id="email" name="email" class="registration_field" maxlength="32">
                </div>
                <input type="submit" name="submit" class="register-button" value="Зарегистрироваться">
            </form>
            <strong><div class="registrations-errors"></div></strong>
        </div>
    </div>
</div>

<script>
    $("#form-register").submit(function(event) {
    event.preventDefault();
    $.post('register-action.php', {
        nickname: document.getElementById("nickname").value,
        password1: document.getElementById("password1").value,
        password2: document.getElementById("password2").value,
        email: document.getElementById("email").value
    }, function(data, textStatus, xhr) {
        switch(data){
            case "error1":
            document.getElementsByClassName("registrations-errors")[0].innerHTML="Пароли не совпадают!";
            break;
            case "error2":
                document.getElementsByClassName("registrations-errors")[0].innerHTML="Вы ввели не всю информацию,<br> заполните все поля!";
            break;
            case "error3":
                document.getElementsByClassName("registrations-errors")[0].innerHTML="Введенный вами ник недопустим!";
            break;
            case "error4":
                document.getElementsByClassName("registrations-errors")[0].innerHTML="Введенный вами email некорректный!";
            break;
            case "error5":
                document.getElementsByClassName("registrations-errors")[0].innerHTML="Извините, введённые вами логин или почта уже зарегистрированы. Введите другие значения.";
            break;
            case "ErrorSmall":
                document.getElementsByClassName("registrations-errors")[0].innerHTML="Введенный вами логин/пароль<br>слишком короткий!";
            break
            case "ErrorBig":
                document.getElementsByClassName("registrations-errors")[0].innerHTML="Введенный вами логин/пароль/email слишком длинный!";
            break;
            case "register":
                document.getElementsByClassName("registrations-errors")[0].innerHTML="Вы успешно зарегистрированы! Теперь вы можете зайти на сайт.";
                document.getElementsByClassName("registrations-errors")[0].style.color="green";
                setTimeout("document.location.href='lc.php#profile'", 250);
            break;
            case "fatal_error":
                document.getElementsByClassName("registrations-errors")[0].innerHTML="Ошибка! Вы не зарегистрированы.";
            break;
        }
    });
    });
</script>