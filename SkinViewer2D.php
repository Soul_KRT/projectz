<?php

if (isset($_GET['show'])) {

    require 'SkinViewer2D.class.php';
    header("Content-type: image/png");
      
    $show = $_GET['show'];
    
    $baseDir = dirname(__FILE__) . '/';
    $skin =  empty($_GET['file_name']) ? 'default' : $_GET['file_name'];
    $skin =  $baseDir . $skin . '.png';

    if (!skinViewer2D::isValidSkin($skin)) {
        $skin = $baseDir . 'default.png';
    }
    
    if ($show !== 'head') {
        $cloak = isset($_GET['cloak']) ? (($_GET['cloak'] === 'hd') ? './skins/cloak.png' : './skins/cloak.png') : false;
        $side = isset($_GET['side']) ? $_GET['side'] : false;

        $img = skinViewer2D::createPreview($skin, $cloak, $side);
    } else {
        $img = skinViewer2D::createHead($skin, 64);
    }

    imagepng($img);
    
}
?>