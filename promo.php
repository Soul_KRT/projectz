<script>
    let path;

    function download(path) {
        var link = document.createElement('a');
        link.setAttribute('href', path);
        link.setAttribute('download', 'ProjectZ');
        link.click();
    }

</script>

<img type="image" src="img/promo-bg.png" class="main-promo_background">
<div class=main-promo_block id="promo">
    <div class="main-promo_text">
        <strong>ПОДПИСЫВАЙТЕСЬ НА НАС ВКОНТАКТЕ</strong>
        <p>Подписывайся и будь в курсе новостей</p>
    </div>
    <img class="main-promo_banner" src="img/promo-banner.png" onclick="download('/favicon.ico')">
</div>