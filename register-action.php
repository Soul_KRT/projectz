<?php
    session_start();
    if(isset($_POST['nickname']) && isset($_POST['password1']) && isset($_POST['password2']) && isset($_POST['email'])){
        $nickname = $_POST['nickname'];
        if ($nickname == '')
        {
            unset($nickname);
        }
        if($_POST['password1'] == $_POST['password2']){
            $password=$_POST['password1'];
            if ($password == '') {
                unset($password);
            }
        } else {
            exit("error1");
            //Пароли не совпадают!
        }
        $email = $_POST['email'];
        if ($email == '')
        {
            unset($email);
        }
    	if (empty($nickname) or empty($password) or empty($email))
        {
            exit ("error2");
            //Вы ввели не всю информацию, заполните все поля!
        }

        $nickname = stripslashes($nickname);
        $nickname = htmlspecialchars($nickname);
        $password = stripslashes($password);
        $password = htmlspecialchars($password);
        $email = stripslashes($email);
        $email = htmlspecialchars($email);

        $nickname = trim($nickname);
        $password = trim($password);
        $email = trim($email);

        if((mb_strlen($nickname) < 4) || (mb_strlen($password) < 6)){
            exit("ErrorSmall");
        }

        if(mb_strlen($nickname) > 16 || mb_strlen($password) > 16 || mb_strlen($email) > 32 ){
            exit("ErrorBig");
        }

        if(!preg_match("/^[A-Za-z][A-Za-z0-9_]{1,16}$/", $nickname)){
            exit("error3");
            //Введенный Вами ник недопустим!
        }

        $email = mb_strtolower($email);

        if(!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/", $email)){

            exit("error4");
            //Введенный Вами email некорректный!
        }

        include ("db.php");

        $result = $db -> query("SELECT id FROM user WHERE nick='$nickname' or mail='$email'");
        $myrow = mysqli_fetch_row($result);
        if ($result -> num_rows > 0) {
            exit ("error5");
            //Извините, введённые Вами логин или почта уже зарегистрированы. Введите другие значения.
        }

        $cript = "87avubof";
        $email_hash = md5($nickname . time() . $cript);
    	$password = md5($password);

        $headers  = "MIME-Version: 1.0\r\n";
                $headers .= "Content-type: text/html; charset=utf-8\r\n";
                $headers .= "To: <$email>\r\n";
                $headers .= "From: <soul.krt.00@mail.ru>\r\n";

                $message = '
                        <html>
                        <head>
                        <title>Подтвердите Email</title>
                        </head>
                        <body>
                        <p>Что бы подтвердить Email, перейдите по <a href="http://projectz/verify.php?mail_hash=' . $email_hash . '">ссылке</a></p>
                        </body>
                        </html>
                        ';
                mail($email, "Подтвердите Email на сайте", $message, $headers);

        $result2 = $db -> query("INSERT INTO user (nick,pass,mail,skin,status,balance,verify,mail_hash,firstjoin) VALUES('$nickname','$password','$email','default','user','0','0','$email_hash','".date('o-m-d')."')");
        $result3 = $db -> query("INSERT INTO session (user_id,session_id) VALUES('$db->insert_id','".SESSION_ID()."')");
        if ($result2=='TRUE' && $result3=='TRUE')
        {
            exit("register");
            //Вы успешно зарегистрированы! Теперь вы можете зайти на сайт.
        } else {
            exit("fatal_error");
            //Ошибка! Вы не зарегистрированы.
        }
    }
?>

<script>
    document.location.href='index.php';
</script>