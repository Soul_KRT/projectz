<?php
	if(isset($_POST['name']) && isset($_POST['type']) && isset($_POST['amount']) && isset($_POST['price']) && isset($_POST['category'])){

		if (!($_FILES['img']['error'] == UPLOAD_ERR_OK && ($_FILES['img']['type'] == 'image/png' || $_FILES['img']['type'] == 'image/jpeg'))) {
        	exit("error_add3");
        }

		$name = $_POST['name'];
		$type = $_POST['type'];
		$amount = $_POST['amount'];
		$price = $_POST['price'];
		$category = $_POST['category'];

        $name = stripslashes($name);
        $name = htmlspecialchars($name);
        $name = trim($name);

        $type = stripslashes($type);
        $type = htmlspecialchars($type);
        $type = trim($type);

        $amount = stripslashes($amount);
        $amount = htmlspecialchars($amount);
        $amount = trim($amount);

        $price = stripslashes($price);
        $price = htmlspecialchars($price);
        $price = trim($price);

        $category = stripslashes($category);
        $category = htmlspecialchars($category);
        $category = trim($category);

        if ($name == '')
        {
            unset($name);
        }

        if ($type == '')
        {
            unset($type);
        }

        if ($amount == '')
        {
            unset($amount);
        }

        if ($price == '')
        {
            unset($price);
        }

        if ($category == '')
        {
            unset($category);
        }

        if(empty($name) or empty($type) or empty($amount) or empty($price) or empty($category)) {
        	exit("error_add1");
        }

        if($amount <= 0 or $price <= 0){
        	exit("error_add2");
        }

        include ("db.php");

        $category_rows = $db -> query("SELECT id FROM category WHERE name='$category'");

        if($category_rows->num_rows == 0){
        	$db -> query("INSERT INTO category (name) VALUES('$category')");
        	$category_id = $db->insert_id;
        } else {
        	$category_id = mysqli_fetch_row($category_rows)[0];
        }

        $result1 = $db -> query("INSERT INTO items (name,category_id,price,type,amount) VALUES('$name','$category_id','$price','$type','$amount')");

        $item_id = $db->insert_id;

        $result2 = $db -> query("UPDATE items set img = '$item_id$name' where id='$item_id'");
        if ($result1=='TRUE' && $result2=='TRUE')
        {
	        $img = "items/".$item_id.$name.".png";
	        move_uploaded_file($_FILES['img']["tmp_name"], $img);
            exit("added");
        }
        else {
            exit("fatal_error");
        }
	}
?>

<script>
    document.location.href='index.php';
</script>