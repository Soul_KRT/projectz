<?php
	if(isset($_POST['user_id']) && isset($_POST['item_id']) && isset($_POST['quantity'])){

		$user_id = $_POST['user_id'];
		$item_id = $_POST['item_id'];
		$quantity = $_POST['quantity'];

        if($user_id == "null"){
            exit("error-user_null");
        }

        $quantity = stripslashes($quantity);
        $quantity = htmlspecialchars($quantity);
        $quantity = trim($quantity);

        if ($quantity == '')
        {
            unset($quantity);
        }

        if(empty($quantity)) {
        	exit("error-buy1");
        }

        if($quantity <= 0 || ((int) $quantity) != ((string) $quantity)){
            exit("error-buy1");
        }

        include ("db.php");

        $price = mysqli_fetch_row($db -> query("SELECT price FROM items WHERE id='$item_id'"))[0];

        $balance = mysqli_fetch_row($db -> query("SELECT balance FROM user WHERE id='$user_id'"))[0];

        $setBalance = $balance - $price*$quantity;

        if($setBalance < 0){
            exit("error-buy2");
        }

        $result1 = $db -> query("UPDATE user set balance = '$setBalance' WHERE id='$user_id'");

        $cart = $db -> query("SELECT id,quantity FROM cart WHERE user_id='$user_id' and item_id='$item_id'");

        if($cart->num_rows > 0){
            $cart = mysqli_fetch_row($cart);
            $quantity += $cart[1];
            $result2 = $db -> query("UPDATE cart set quantity = '$quantity' WHERE id='$cart[0]'");
        } else {
            $result2 = $db -> query("INSERT INTO cart (user_id,item_id,quantity) VALUES('$user_id','$item_id','$quantity')");
        }

        if ($result1=='TRUE' && $result2=='TRUE')
        {
            exit("buyed");
        }
        else {
            exit("fatal_error");
        }
	}
?>

<script>
    document.location.href='index.php';
</script>