<?php
    session_start();
    $id = SESSION_ID();
?>
<meta charset="UTF-8">
<link rel="stylesheet" href="reset.css?<?php echo filemtime('reset.css') ?>"/>
<link rel="stylesheet" href="header.css?<?php echo filemtime('header.css') ?>"/>
<link rel="stylesheet" href="footer.css?<?php echo filemtime('footer.css') ?>"/>
<link rel="stylesheet" href="main.css?<?php echo filemtime('main.css') ?>"/>
<link rel="stylesheet" href="start.css?<?php echo filemtime('start.css') ?>"/>
<title>ProjectZ</title>
<?php
    include("header.php");
    include("promo.php");
?>

<script>
    function After(){
        var login_block = document.getElementsByClassName("login-block")[0];
        login_block.style.transform = "scale(1.1)";
    }

    function Before(){
        var login_block = document.getElementsByClassName("login-block")[0];
        login_block.style.transform = "scale(1.0)";
    }

    function ViewLogin(){
        var login_block = document.getElementsByClassName("login-block")[0];
        location.href='#promo';
        login_block.style.transition = "transform .2s cubic-bezier(0.68, -3.67, 0.37, 3.98)";
        setTimeout("After()", 100);
        setTimeout("Before()", 350);
    }
</script>

<div class="main">
    <div class="start">
        <div class="start-block start-block_first">
            <span class="start-block-title">1. Создайте аккаунт</span>
            <p>Первым делом Вам необходимо создать аккаунт на нашем сайте. Перед регистрацией рекомендуем ознакомиться с правилами сервера.</p>
            <br>
            <p>Если у Вас уже есть аккаунт на нашем сайте, то воспользуйтесь формой авторизации.</p>
            <br>
            <div class="start-button" onclick="location.href='/registration-check.php'">Зарегистрироваться</div>
            <div class="start-button" onclick="ViewLogin()">Войти в аккаунт</div>
        </div>
        <div class="start-block">
            <span class="start-block-title">2. Установите лаунчер</span>
            <p>По причиние того, что мы используем собственные моды, вход на сервер осуществляется только через наш лаунчер, который можно скачать ниже. Он сам загрузит, установит и настроит клиент для игры.</p>
            <br>
            <p>Для работы лаунчера требуется .NET Framework 4.0 или выше. В случае необходимости можно скачать с официального сайта Microsoft.</p>

            <div class="start-button" onclick="download('/favicon.ico')">Скачать</div>
        </div>
        <div class="start-block">
        <span class="start-block-title">3. Играйте</span>
            <p>После загрузки Вам необходимо запустить лаунчер и ввести там свой логин и пароль, которые Вы указали при регистрации. Дальше просто выберите сервер и играйте!</p>
            <br>
            <p>Рекомендуем сразу подписаться на наше сообщество в VK. Именно там Вы можете раньше всех узнать про акции и обновления.</p>
            <br>
            <script type="text/javascript" src="https://vk.com/js/api/openapi.js?168"></script>

            <div id="vk_groups" class="vk-widget"></div>
            <script type="text/javascript">
            VK.Widgets.Group("vk_groups", {mode: 3,width: "0"}, 125025753);

            </script>
        </div>
    </div>

    <?php
        include("db.php");
        include("sidebar.php");
    ?>

</div>

<?php
    include("footer.php");
?>