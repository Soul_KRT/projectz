<?php
    if(isset($_POST['user_id']) && isset($_POST['old_pass']) && isset($_POST['pass1']) && isset($_POST['pass2'])) {
        $user_id = $_POST['user_id'];

        include ("db.php");

        $pswd = $db -> query("SELECT pass FROM user WHERE id='$user_id'");
        $pswd_row = mysqli_fetch_row($pswd);
        $true_pass = $pswd_row[0];

        $old_pass = $_POST['old_pass'];
        if ($old_pass == '')
        {
            unset($old_pass);
        }

        if($_POST['pass1'] == $_POST['pass2']){
            $password = $_POST['pass1'];
            if ($password == '') {
                unset($password);
            }
        } else {
            exit("error3");
            //Новые пароли не совпадают!
        }

    	if (empty($old_pass) or empty($password))
        {
            exit ("error1");
            //Вы ввели не всю информацию, заполните все поля!
        }

        $old_pass = stripslashes($old_pass);
        $old_pass = htmlspecialchars($old_pass);
        $password = stripslashes($password);
        $password = htmlspecialchars($password);

        $old_pass = trim($old_pass);
        $password = trim($password);

        $old_pass = md5($old_pass);

        if($old_pass != $true_pass){
            exit ("error2");
            //Неверный старый пароль!
        }

        if((strlen($password) < 6)) {
            exit("ErrorSmall");
        }

        if((strlen($password) > 16)) {
            exit("ErrorBig");
        }

        $password = md5($password);

        $result = $db -> query("UPDATE user set pass = '$password' where id='$user_id'");
        if ($result=='TRUE')
        {
            exit("confirm");
            //Пароль успешно изменён!
        }
        else {
            exit("fatal_error");
            //Ошибка! Пароль не изменён.
        }
    }
?>

<script>
    document.location.href='index.php';
</script>