<?php
    session_start();
    $id = SESSION_ID();

    include("db.php");
    $result_check = $db -> query("SELECT user_id FROM session WHERE session_id='$id'");
    $checkrow = mysqli_fetch_row($result_check);
    if($result_check->num_rows != 0){
        $user_id = $checkrow[0];
    }
    if (!isset($user_id)){
        exit("<script>
            document.location.href='index.php'
        </script>");
    }

    $user_status = $db -> query("SELECT status FROM user WHERE id='$user_id'");

    $status = mysqli_fetch_row($user_status)[0];

    if ($status != "admin"){
        exit("<script>
            document.location.href='index.php'
        </script>");
    }

    $category = $db -> query("SELECT name FROM category");
    ini_set('upload_max_filesize', '1M');

?>
<meta charset="UTF-8">
<link rel="stylesheet" href="reset.css?<?php echo filemtime('reset.css') ?>"/>
<link rel="stylesheet" href="header.css?<?php echo filemtime('header.css') ?>"/>
<link rel="stylesheet" href="main.css?<?php echo filemtime('main.css') ?>"/>
<link rel="stylesheet" href="footer.css?<?php echo filemtime('footer.css') ?>"/>
<link rel="stylesheet" href="item-form.css?<?php echo filemtime('main.css') ?>"/>
<title>ProjectZ</title>
<?php
	include("header.php");
	include("promo.php");
?>
<div class="main">
	<div class="content">
		<div class="item_form">
	        <p class="item_form-text">Добавление товара</p>
			<div class="item_form-block">
				<div class="item_form-img_block">
					<label style="margin: auto;" for="img">
					<img class="item_form-img" src="items/default.png"></label>
					<p class=item_form-img_text>128 x 128</p>
				</div>
				<div class="item_form-data">
					<form method="post" id="form" action="">
			            <p class="field_lable_black">Название:</p>
			            <p><input class="item_form-field" id="name" type="text" name="name" maxlength="32"></p>
			            <p class="field_lable_black">Предмет:</p>
			            <p><input class="item_form-field" id="type" type="text" name="type" maxlength="32"></p>
			            <p class="field_lable_black">Количество:</p>
			            <p><input class="item_form-field" id="amount" type="number" name="amount" min="1" maxlength="10" placeholder="1"></p>
			            <p class="field_lable_black">Цена:</p>
			            <p><input class="item_form-field" id="price" type="number" name="price" min="0.1" step="0.01" placeholder="0,1" maxlength="10"></p>
			            <p class="field_lable_black">Категория:</p>
			            <p><input list="category" class="item_form-field category" name="category" autocomplete="off" maxlength="32">
						   <datalist id="category">
						    <?php
						    	while($key = mysqli_fetch_row($category)) {
						    		echo "<option value=\"$key[0]\">";
						    	}
						    ?>
						   </datalist>
						</p>
						<p><input style="display: none;" name="img" type="file" id="img" accept="image/png, image/jpeg"></p>
				        <input type="submit" class="item_form-button" value="Подтвердить">
			        </form>
			        <strong><div class="add-errors"></div></strong>
		    	</div>
		    </div>
	    </div>
	</div>
<?
    include("sidebar.php");
?>
</div>
<?php
    include("footer.php");
?>

<script>
	$("#form").submit(function(event) {
    event.preventDefault();
    $.ajax({
      url: 'item-add.php',
      type: 'POST',
      data: new FormData(this),
      processData: false,
      contentType: false,
      complete: function(data) {
        switch(data.responseText){
            case "error_add1":
                document.getElementsByClassName("add-errors")[0].innerHTML="Заполните все поля!";
            break;
            case "error_add2":
                document.getElementsByClassName("add-errors")[0].innerHTML="Неверная цена или количество!";
            break;
            case "error_add3":
                document.getElementsByClassName("add-errors")[0].innerHTML="Загрузите валидную фотографию товара!";
            break;
            case "added":
                document.getElementsByClassName("add-errors")[0].innerHTML="Товар успешно добавлен!";
                document.getElementsByClassName("add-errors")[0].style.color="green";
                setTimeout("window.location.reload()", 250);
            break;
        	case "fatal_error":
                document.getElementsByClassName("add-errors")[0].innerHTML="Ошибка! Товар не добавлен.";
            break;
        }
      }
    });
  	});
  $("#img").on("change", function() {
    if (this.files[0]) {      var fr = new FileReader();
      $(".item_form-img").fadeOut(0);
      fr.addEventListener("load", function() {
        document.querySelector(".item_form-img").src = fr.result;
        $(".item_form-img").fadeIn(200);

      });

      fr.readAsDataURL(this.files[0]);
    }
  });

  var oldValue = '';
  $(".category").on('click', function() {
    oldValue = $(this).val();
    $(this).val('');
  });
  $('input').on('mouseleave', function() {
    if ($(this).val() == '') {
      $(this).val(oldValue);
    }
  });
</script>